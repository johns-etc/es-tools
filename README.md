# mini tools

[Black Border Detection and Cropping](crop_black_borders_notebook.ipynb)

This Jupyter notebook demonstrates the steps to detect black borders in an image and crop the contents within those borders. The process includes converting the image to grayscale, detecting edges, applying Hough transform to find lines, and identifying the rectangular contours which likely represent the borders.