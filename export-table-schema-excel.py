import mysql.connector
import pandas as pd
import os

# 数据库连接配置
config = {
    'user': os.environ['EXP_DB_USER'],
    'password': os.environ['EXP_DB_PASSWORD'],
    'host': os.environ['EXP_DB_HOST'],
    'database': os.environ['EXP_DB_NAME'],
    'raise_on_warnings': True
}

# 连接到MySQL数据库
cnx = mysql.connector.connect(**config)
cursor = cnx.cursor()

# 创建输出目录，如果它不存在的话
output_dir = f"output/{config['database']}"
if not os.path.exists(output_dir):
    os.makedirs(output_dir)

# 获取所有表的名称和注释
query = """
SELECT TABLE_NAME, TABLE_COMMENT 
FROM information_schema.tables 
WHERE table_schema = %s
"""
cursor.execute(query, (config['database'],))
tables = cursor.fetchall()

for table_name, table_comment in tables:
    # 获取表的字段信息
    column_query = """
    SELECT COLUMN_NAME, COLUMN_TYPE, IS_NULLABLE, COLUMN_KEY, COLUMN_DEFAULT, EXTRA, COLUMN_COMMENT 
    FROM information_schema.COLUMNS 
    WHERE TABLE_SCHEMA = %s AND TABLE_NAME = %s
    """
    cursor.execute(column_query, (config['database'], table_name))
    columns_info = cursor.fetchall()

    # 创建DataFrame
    df_table = pd.DataFrame([{"Table Name": table_name, "Comment": table_comment}])
    df_columns = pd.DataFrame(columns_info, columns=["Field", "Type", "Null", "Key", "Default", "Extra", "Comment"])

    # 将数据写入Excel文件
    file_path = os.path.join(output_dir, f"{table_name}.xlsx")
    with pd.ExcelWriter(file_path) as writer:
        df_table.to_excel(writer, sheet_name='Table Description', index=False)
        df_columns.to_excel(writer, sheet_name='Field Description', index=False)

# 关闭数据库连接
cursor.close()
cnx.close()
