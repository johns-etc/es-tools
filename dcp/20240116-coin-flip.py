import math

def expected_rounds(n):
    if n <= 0:
        raise ValueError("Number of coins must be positive")
    return math.ceil(math.log2(n))

# Example usage:
n = 6  # Replace with any positive integer
print("Expected number of rounds for", n, "coins:", expected_rounds(n))
