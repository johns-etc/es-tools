import mysql.connector
import os
from docx import Document


# 数据库连接配置
config = {
    'user': os.environ['EXP_DB_USER'],
    'password': os.environ['EXP_DB_PASSWORD'],
    'host': os.environ['EXP_DB_HOST'],
    'database': os.environ['EXP_DB_DATABASE'],
    'raise_on_warnings': True
}

# 连接到MySQL数据库
cnx = mysql.connector.connect(**config)
cursor = cnx.cursor()

# 创建输出目录，如果它不存在的话
output_dir = "output"
if not os.path.exists(output_dir):
    os.makedirs(output_dir)

# 创建Word文档
doc = Document()

# 获取所有表的名称和注释
query = """
SELECT TABLE_NAME, TABLE_COMMENT 
FROM information_schema.tables 
WHERE table_schema = %s
"""
cursor.execute(query, (config['database'],))
tables = cursor.fetchall()

for table_name, table_comment in tables:
    doc.add_heading(f'Table: {table_name}', level=1)
    doc.add_paragraph(f'Comment: {table_comment}')

    # 获取表的字段信息
    column_query = """
    SELECT COLUMN_NAME, COLUMN_TYPE, IS_NULLABLE, COLUMN_KEY, COLUMN_DEFAULT, EXTRA, COLUMN_COMMENT 
    FROM information_schema.COLUMNS 
    WHERE TABLE_SCHEMA = %s AND TABLE_NAME = %s
    """
    cursor.execute(column_query, (config['database'], table_name))
    columns_info = cursor.fetchall()

    # 将字段信息添加到Word文档中
    doc.add_heading('Fields:', level=2)
    table = doc.add_table(rows=1, cols=7)
    table.style = 'Table Grid'
    hdr_cells = table.rows[0].cells
    hdr_cells[0].text = 'Field'
    hdr_cells[1].text = 'Type'
    hdr_cells[2].text = 'Null'
    hdr_cells[3].text = 'Key'
    hdr_cells[4].text = 'Default'
    hdr_cells[5].text = 'Extra'
    hdr_cells[6].text = 'Comment'

    for row in columns_info:
        row_cells = table.add_row().cells
        for i, value in enumerate(row):
            row_cells[i].text = str(value)

    # 在每个表的章节后添加分页符
    doc.add_page_break()

# 保存文档
doc.save(os.path.join(output_dir, f'{config["database"]}.docx'))

# 关闭数据库连接
cursor.close()
cnx.close()
